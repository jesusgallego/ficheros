package es.ua.eps.ficheros;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    SharedPreferences prefs;
    String location;
    String filename;
    final String log_filename = "log.txt";

    Button moveSD;
    Button movePhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        moveSD = (Button) findViewById(R.id.move_sd_button);
        movePhone = (Button) findViewById(R.id.move_phone_button);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);

        toggleButtons();
    }

    private void toggleButtons() {
        location = prefs.getString("location", "phone");
        filename = prefs.getString("filename", "default");

        moveSD.setEnabled(!location.equals("sd"));
        movePhone.setEnabled(!location.equals("phone"));
    }

    private void writeLog(String text) {
        if (location.equals("phone")) {
            try {
                OutputStreamWriter fout = new OutputStreamWriter(openFileOutput(log_filename, Context.MODE_APPEND));

                fout.write(String.format("%s\n", text));
                fout.close();
            } catch (Exception e) {
                Log.e("Ficheros", "Error escribiendo en memoria interna");
            }
        } else if (location.equals("sd")) {
            try {
                if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                    File rutaSD = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
                    rutaSD.mkdirs();
                    File f = new File(rutaSD.getAbsolutePath() + File.separator + log_filename);

                    OutputStreamWriter fout = new OutputStreamWriter(new FileOutputStream(f, true));

                    fout.write(String.format("%s\n", text));
                    fout.close();
                }
            } catch (Exception ex) {
                Log.e("Ficheros", "Error al escribir fichero a tarjeta SD");
                ex.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onExternalStorageClicked(View v) {
        writeLog("External Storage clicked");
        Intent intent = new Intent(this, EnvironmentActivity.class);
        startActivity(intent);
    }

    public void onSeeLogClicked(View v) {
        writeLog("See Log clicked");

        String log = "";

        if (location.equals("phone")) {

            try {
                BufferedReader fin = new BufferedReader(new InputStreamReader(openFileInput(log_filename)));
                String line = null;
                while ((line = fin.readLine()) != null) {
                    log += line + "\n";
                }

                fin.close();
            } catch (FileNotFoundException e) {
                Log.e("Ficheros", "Error al leer desde memoria interna");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (location.equals("sd")) {
            try {
                File path = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
                File file = new File(path.getAbsolutePath(), log_filename);

                BufferedReader fin = new BufferedReader(new FileReader(file));

                path.mkdirs();

                String line = null;
                while ((line = fin.readLine()) != null) {
                    log += line + "\n";
                }

                fin.close();
            } catch (Exception ex) {
                Log.e("Ficheros", "Error al leer desde tarjeta SD");
            }
        }

        Intent intent = new Intent(this, LogActivity.class);
        intent.putExtra("log", log);
        startActivity(intent);

    }

    public void onAddToFileClicked(View v) {
        writeLog("Add to file clicked");
    }

    public void onSeeFileClicked(View v) {
        writeLog("See file clicked");
    }

    public void onMoveToSDClicked(View v) {
        writeLog("Move files to SD");
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString("location", "sd");
        editor.apply();
    }

    public void onMoveToPhoneClicked(View v) {
        writeLog("Move files to Phone");
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString("location", "phone");
        editor.apply();
    }

    public void onCloseClicked(View v) {
        writeLog("-- App Closed --");
        finish();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        writeLog("Preferences changed");
        toggleButtons();
    }
}
