package es.ua.eps.ficheros;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by mastermoviles on 9/11/16.
 */
public class SettingsActivity extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
    }
}
