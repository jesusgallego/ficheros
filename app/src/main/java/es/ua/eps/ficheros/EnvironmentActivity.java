package es.ua.eps.ficheros;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

public class EnvironmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_environment);

        CheckBox media_unknown = (CheckBox) findViewById(R.id.media_unknown);
        CheckBox media_removed = (CheckBox) findViewById(R.id.media_removed);
        CheckBox media_unmounted = (CheckBox) findViewById(R.id.media_unmounted);
        CheckBox media_checking = (CheckBox) findViewById(R.id.media_checking);
        CheckBox media_nofs = (CheckBox) findViewById(R.id.media_nofs);
        CheckBox media_mounted = (CheckBox) findViewById(R.id.media_mounted);
        CheckBox media_mounted_read_only = (CheckBox) findViewById(R.id.media_mounted_read_only);
        CheckBox media_shared = (CheckBox) findViewById(R.id.media_shared);
        CheckBox media_bad_removal = (CheckBox) findViewById(R.id.media_bad_removal);
        CheckBox media_unmountable = (CheckBox) findViewById(R.id.media_unmountable);

        String estado = Environment.getExternalStorageState();

        media_unknown.setChecked(estado.equals(Environment.MEDIA_UNKNOWN));
        media_removed.setChecked(estado.equals(Environment.MEDIA_REMOVED));
        media_unmounted.setChecked(estado.equals(Environment.MEDIA_UNMOUNTED));
        media_checking.setChecked(estado.equals(Environment.MEDIA_CHECKING));
        media_nofs.setChecked(estado.equals(Environment.MEDIA_NOFS));
        media_mounted.setChecked(estado.equals(Environment.MEDIA_MOUNTED));
        media_mounted_read_only.setChecked(estado.equals(Environment.MEDIA_MOUNTED_READ_ONLY));
        media_shared.setChecked(estado.equals(Environment.MEDIA_SHARED));
        media_bad_removal.setChecked(estado.equals(Environment.MEDIA_BAD_REMOVAL));
        media_unmountable.setChecked(estado.equals(Environment.MEDIA_UNMOUNTABLE));

    }

    public void onVolverClicked(View v) {
        finish();
    }
}
